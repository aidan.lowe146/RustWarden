use std::io::stdout;
use std::process::exit;
use std::{fs, env};

use crossterm::event::{read, Event, KeyEvent, KeyCode, EnableMouseCapture, KeyModifiers, KeyEventKind, MouseEventKind, MouseEvent, MouseButton};
use crossterm::ExecutableCommand;
use crossterm::terminal::{size, window_size};

struct ScreenData {
    lines: Vec<String>,
    offset: i32,
    cur_column: i32,
    cur_row: i32,
    term_height: i32,
    term_width: i32,
    mode: String
}

fn main() -> std::io::Result<()> {
    let mut stdout = stdout();
    let _ = stdout.execute(EnableMouseCapture);
    let mut screen: ScreenData = ScreenData {
        lines: Vec::new(),
        offset: 0,
        cur_column: 0,
        cur_row: 0,
        term_width: size().unwrap().0 as i32,
        term_height: size().unwrap().1 as i32,
        mode: "File Directory".to_string()
    };
    
    loop {
        match read()? {
            Event::FocusGained => (),
            Event::FocusLost => (),
            Event::Key(event) => key_pressed_handle(event),
            Event::Mouse(event) => mouse_event_handle(event),
            Event::Paste(data) => println!("Paste{:?}", data),
            Event::Resize(width, height) => update_size(&mut screen)
        };
        update_display(&mut screen);
    }
}

fn update_display(screen: &mut ScreenData) {
    screen.lines.clear();
    let display_lines = match screen.mode.as_str() {
        "File Directory" => file_directory_display(screen),
        _ => "None".to_string()
    };
    print!("\x1b[1;1H{}some stuff", display_lines);
}

fn update_size(screen: &mut ScreenData) {
    screen.term_width = size().unwrap().0 as i32;
    screen.term_height = size().unwrap().1 as i32;
}

fn file_directory_display(screen: &mut ScreenData) -> String {
    let mut directory_entries: Vec<_> = fs::read_dir(".").unwrap().map(|res| res.unwrap().path()).collect();
    directory_entries.sort();
    screen.lines.push(env::current_dir().unwrap().display().to_string());
    let mut display_line: String;
    for display_row in 0..(screen.term_height - 1) {
        if directory_entries.len() > (display_row + screen.offset) as usize {
            display_line = directory_entries[(display_row + screen.offset) as usize].file_name().unwrap().to_str().unwrap().to_string();
            display_line += " ".repeat(screen.term_width as usize - display_line.len()).as_str();
            display_line;
        } else {
            display_line = " ".repeat(screen.term_width as usize).to_string()
        };
        screen.lines.push(display_line);
        display_line.clear();
    }
    return screen.lines.join("\n").to_string();
}

fn key_pressed_handle(event: KeyEvent) {
    if event.kind == KeyEventKind::Press {
        match event.code {
            KeyCode::Char(ind_char) => if event.modifiers == KeyModifiers::empty() { character_key(ind_char) }
                                            else { modded_key(ind_char, event.modifiers) },
            KeyCode::Backspace => backspace_key(),
            KeyCode::Enter => enter_key(),
            KeyCode::Tab => indent_key(),
            KeyCode::BackTab => dedent_key(),
            KeyCode::Esc => esc_key(),
            KeyCode::Home => test_key(),
            _ => ()
        }
    }
}

fn mouse_event_handle(event: MouseEvent) {
    if event.kind == MouseEventKind::Down(MouseButton::Left) {
        println!("Clicked left button at x{} y{}", event.column, event.row)
    } else if event.kind == MouseEventKind::Down(MouseButton::Right) {
        println!("Clicked right button at x{} y{}", event.column, event.row)
    }
}

fn character_key(inp_char: char) {
    println!("Char {}", inp_char);
}

fn modded_key(inp_char: char, modifiers: KeyModifiers) {
    if modifiers == KeyModifiers::SHIFT {
        character_key(inp_char)
    } else {
        println!("Char {}, modifiers {:?}", inp_char, modifiers);
    }
}

fn backspace_key() {}

fn enter_key() {}

fn indent_key() {}

fn dedent_key() {}

fn esc_key() { exit(0) }

fn test_key() {
    for number in 1..4 {
        println!("{}", number)
    }
}